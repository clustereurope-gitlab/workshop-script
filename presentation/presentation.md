autoscale: true
slidenumbers: true
theme: Plain Jane

![inline](logo.png)

# Building deployment pipeline

### Kamil Trzciński, CI/CD Lead

---

<br>

![inline](https://wiki.jenkins-ci.org/download/attachments/2916393/logo.png?version=1&modificationDate=1302753947000) ![inline](concourse.jpg)

<br>

![inline](http://docs.travis-ci.com/images/travis-mascot-200px.png) ![inline](bitbucket.png)

<br>

---

![inline](pipelines_everywhere_2.jpg)

> http://es.memegenerator.net/instance/61228673

---

> In software engineering, **a pipeline consists of a chain of processing elements** (processes, threads, coroutines, functions, etc.), arranged so that the output of each element is the input of the next; the name is by analogy to a physical pipeline
-- https://en.wikipedia.org/wiki/Pipeline_(software)

---

## Let's talk about CI/CD

---

![inline](gitlab-ci-scope.png)

---

![inline](types-of-pipelines.png)

---

## What we gonna do?

---

### Build > (Test) > Review > Staging > Production

---

## Our pipeline

1. **Build** to create a docker image of our application,
1. **Review** to deploy a preview version,
1. **Staging** to deploy almost production version,
1. **Production** to deploy production

---

## How we gonna achieve that?

1. **Use `Dockerfile`**: to define how to build application
1. **Use `Kubernetes`**: to ease a deployment and management
1. **Use `GitLab`**: for hosting source code, and run CI/CD pipeline

---

![inline, 10%](kubernetes.png)

---

## Kubernetes

1. A platform for hosting Docker containers in clustered environment with multiple Docker hosts,
2. Provides container grouping, load balancing, auto-healing, scaling features
3. Started by Google

---

## Concepts of Kubernetes

1. **Pod** a group of containers,
2. **Deployment** describes how to spawn Pod,
3. **Service** describes what is exposed by Pod (from network perspective),
4. **ReplicaSet** manages lifecycle of Pod,
5. **Ingress** describes externally reachable URLs.
6. There's more...

---

## Hands-on

### https://gitlab.com/clustereurope-gitlab

- Workshops Script
- Minimal Ruby App
- Complete Ruby App

---

![inline](group_qrcode.png)

---

![inline](deployboard.png)

---

![inline](monitoring.png)

---

![inline,30%](logo.png)

## We are hiring!

### https://about.gitlab.com/jobs/

---

## **Thanks!**

## Building deployment pipeline
