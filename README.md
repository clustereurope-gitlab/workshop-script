## GitLab Kubernetes deployment Pipeline

This document describes steps to convert an application into working Kubernetes deployment using GitLab and GitLab Pipelines.

Materials:
- [Kubernetes, Production-Grade Container Orchestration](https://kubernetes.io/),
- [Docker, Build, Ship and Run Any App, Anywhere](https://www.docker.com/),
- [GitLab CI](https://docs.gitlab.com/ce/ci/),
- [GitLab Environments](https://docs.gitlab.com/ce/ci/environments.html#defining-environments),
- [GitLab Kubernetes Integration](https://docs.gitlab.com/ce/user/project/integrations/kubernetes.html),
- [Kubernetes Nginx Ingress](https://github.com/jetstack/kube-lego/tree/master/examples/nginx)

## 1. Dockerize application

Usually, your repository has only sources.
You need to "Dockerize" your application,
it means to prepare a `Dockerfile` that will allow to build and ship your application.

For simple single file Ruby application using WEBRick, you would create the following Dockerfile:

```Dockerfile
FROM ruby:2.4.0-alpine
ADD ./ /app/
WORKDIR /app
EXPOSE 5000

CMD ["ruby", "./server.rb"]
```

### 1.1. Dockerfile's FROM

We use `ruby:2.4.0-alpine` as a base image, because of it's size: 60.8MB. You can use any other image, but keep in mind that using images that weight 900MB will cause a longer build and push times, but also longer deployments.

To check image size do following:
```bash
$ docker pull ruby:2.4.0-alpine
...
$ docker images ruby:2.4.0-alpine
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ruby                2.4.0-alpine        18dbdfdbe6bd        8 weeks ago         60.8 MB
```

### 1.2. Dockerfile's ADD/WORKDIR

We copy all sources from the current directory (also including `server.rb`) to the `/app` directory.

We make this directory as a WORKDIR.

### 1.3. Dockerfile's EXPOSE

We expose a port 5000 as we assume that our WEBRick server does run on that specific port.

5000 is a port that is also used later in Kubernetes configuration.

### 1.4. Ensuring that application works

Always you have to build your application first:

```bash
$ docker build -t my-application .
```

Then you can run it:

```bash
$ docker run -p 5000:5000 my-application
```

You should now see on your console an output from your application
and if you go to `http://localhost:5000` you should see the output from it.

## 2. Build application image on GitLab

Let's start by defining GitLab Pipeline.
GitLab Pipeline is stored in `.gitlab-ci.yml` file.
You can read more about syntax and possible options of `.gitlab-ci.yml` in [the documentation](https://docs.gitlab.com/ce/ci/yaml/README.html).

Let's first build our image:

```yaml
# Build application image and push that to GitLab Container Registry
app_image:
  stage: build
  image: docker:git
  services:
    - docker:dind
  variables:
    DOCKER_DRIVER: overlay
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker build -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" .
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
```

We use here a docker-in-docker approach which is one of the few, but it is the only one that is supported on GitLab.com's Shared Runners.
You can read more about another approach for building Docker Images in [the documentation](https://docs.gitlab.com/ce/ci/docker/using_docker_build.html).

Here, we:
1. Define job `app_image`,
2. Put the job in [`build` stage](https://docs.gitlab.com/ce/ci/yaml/README.html#stage),
3. Use a [`docker:git` image](https://hub.docker.com/_/docker/) for our `app_image` job container,
4. Use a [`docker:dind` as service](https://docs.gitlab.com/ce/ci/docker/using_docker_build.html#use-docker-in-docker-executor),
5. Use a [`overlay` to speed-up docker builds](https://docs.gitlab.com/ce/ci/docker/using_docker_build.html#using-the-overlayfs-driver),
6. Build and push image to [GitLab Container Registry](https://docs.gitlab.com/ce/ci/docker/using_docker_build.html#using-the-gitlab-container-registry),
7. Use a [CI_COMMIT_REF_SLUG](https://docs.gitlab.com/ce/ci/variables/README.html#predefined-variables-environment-variables) variable as this is a docker-compatible tag name.

However, this approach doesn't make use of Docker Layer Caching. So, every time everything has to be built from scratch.
It doesn't make a lot of difference for small images, but if you have to compile Ruby dependencies this is not the most effective way. As a side story: Try to extend that script with the use of a newly introduced `--cache-from` syntax of `docker build`.

## 3. See a GitLab Pipeline and build Docker image

Now, go to GitLab and create a new project: https://gitlab.com/projects/new.

With the previously learned Git commands commit your newly created `.gitlab-ci.yml` and push it with your application sources to GitLab.

Go to `Pipelines` and see that your `app_image` is, in fact, building.

The GitLab Pipeline will be run by free GitLab.com Shared Runners.
So you don't really need to provide your own runners to do testing or deployments.

Go to `Registry` and see that your `app_image` is pushed to GitLab Container Registry.

## 4. Create a Kubernetes deployment

Kubernetes deployments consist of different components:
1. [Namespace](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/) - this is where we group similar applications or similar environments,
2. [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) - this is how we describe how to run our application and how many instances of our application we do need,
3. [Service](https://kubernetes.io/docs/concepts/services-networking/service/) - this is how we describe how to access our application, as service can be exposed to the internet, but also to other applications running in cluster,
4. [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/) - this is how we allow our services to be accessible under human readable URLs.

### 4.1. Let's add Deployment to Pipeline

To our `.gitlab-ci.yml` add a `production` job:

```yaml
variables:
  # Use your own `-my-name`,
  # don't use dot as DNS will not work then
  DOMAIN: -my-name.clustereurope-gitlab.xyz

production:
  stage: deploy
  image: alpine
  before_script:
    - apk add -U openssl
    - wget -O /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v1.7.0-alpha.3/bin/linux/amd64/kubectl
    - chmod +x /usr/local/bin/kubectl
  script:
    - sh scripts/deploy "$CI_ENVIRONMENT_SLUG" "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" production$DOMAIN
  environment:
    name: production
    url: http://production$DOMAIN
```

What is happening here:

1. Define job `production`,
2. Put the job in [`deploy` stage](https://docs.gitlab.com/ce/ci/yaml/README.html#stage),
3. Use a [`alpine` image](https://hub.docker.com/_/alpine/) for our `production` job container,
4. Download `kubectl` in [before_script](https://docs.gitlab.com/ce/ci/yaml/README.html#before_script-and-after_script) that is needed to interact with Kubernetes,
5. Define [`environment`](https://docs.gitlab.com/ce/ci/environments.html#defining-environments) that will allow GitLab to discover that this is special purpose job that interacts with Kubernetes cluster,
6. Call a `scripts/deploy` with unique Kubernetes compatible [`$CI_ENVIRONMENT_SLUG`](https://docs.gitlab.com/ce/ci/variables/README.html#predefined-variables-environment-variables), with the name of Docker Image that will be deployed and the hostname under which application should be accessible.

### 4.2. Add a `scripts/deploy`

This is a special script that handles all communication with Kubernetes and does an actual deployment.

Since we will be using [GitLab Kubernetes Integration](https://docs.gitlab.com/ce/user/project/integrations/kubernetes.html) we will use special purpose variables `KUBE_URL`, `KUBE_TOKEN`, `KUBE_CA_PEM_FILE` to interact with Kubernetes cluster. Having that integration enabled allows us to later use [Deploy Boards](https://docs.gitlab.com/ee/user/project/deploy_boards.html) and [Performance Monitoring](https://docs.gitlab.com/ee/administration/monitoring/performance/introduction.html).

```bash
#!/bin/sh

set -e

if [[ $# -ne 3 ]]; then
  echo "usage: $0 <app-name> <app-image> <app-domain>"
  exit 1
fi

app_name="$1"
app_image="$2"
app_domain="$3"
```

First lines of our file do define a required arguments.

### 4.3. Access Kubernetes cluster from GitLab Runner (add to `scripts/deploy`)

To our `scripts/deploy` add a code that will allow us to interact with Kubernetes:

```bash
echo "Storing Kubernetes CA..."
kubectl config set-cluster gitlab-deploy --server="$KUBE_URL" \
  --certificate-authority="$KUBE_CA_PEM_FILE"

echo "Storing Kubernetes Credentials..."
kubectl config set-credentials gitlab-deploy --token="$KUBE_TOKEN"

echo "Defining and using a new Kubernetes configuration..."
kubectl config set-context gitlab-deploy \
  --cluster=gitlab-deploy --user=gitlab-deploy \
  --namespace="$KUBE_NAMESPACE"

kubectl config use-context gitlab-deploy
```

We use strong verification of Kubernetes server, so this is why we enforce TLS server verification to ensure that we sent authorization token to trusted endpoint. The next line does define credentials that will be used, in our case it is token, but it is correct to also use basic-auth or any other type of credentials (client certificates). And finally, we tie cluster with credentials and use that context.

### 4.4. Create namespace (add to `scripts/deploy`)

We should always ensure as part of our scripts that there's a namespace that we can use. The namespace used will be the one that is defined in [GitLab Kubernetes Integration](https://docs.gitlab.com/ce/user/project/integrations/kubernetes.html).

```bash
echo "Creating namespace..."
cat <<EOF | kubectl apply -f -
kind: Namespace
apiVersion: v1
metadata:
  name: $KUBE_NAMESPACE
EOF
```

### 4.5. Create deployment (add to `scripts/deploy`)

The deployment describes how to run application pods.
You can run multiple pods, and within every pod run multiple containers.

```yaml
echo "Creating deployment to run application pods..."
cat <<EOF | kubectl apply --force -n $KUBE_NAMESPACE -f -
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: $app_name
  labels:
    app: $app_name
    job_id: "$CI_JOB_ID"
spec:
  replicas: 1
  template:
    metadata:
      labels:
        name: $app_name
        app: $app_name
        job_id: "$CI_JOB_ID"
    spec:
      containers:
      - name: app
        image: $app_image
        ports:
        - name: web
          containerPort: 5000
EOF
```

You can read more about Kubernetes Deployment definition in this [document](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/).

In that deployment, we use previously passed to script `app_name` and `app_image`. As described in the beginning we expect that our application runs a web server on port `5000`. This number can be changed to any other number.

### 4.6. Run service (add to `scripts/deploy`)

Service is required to group a set of pods. For example, by setting replicas to 10, we would make our application to run in 10 copies. However, without Service, we don't really know how to access our application's web server.

Services are usually not used when you have containers that do background jobs, ex. sidekiq as these containers doesn't have a web server running.

```bash
echo "Creating service to expose 5000 of application pods..."
cat <<EOF | kubectl apply --force -n $KUBE_NAMESPACE -f -
apiVersion: v1
kind: Service
metadata:
  name: $app_name
  labels:
    app: $app_name
    job_id: "$CI_JOB_ID"
spec:
  ports:
    - name: web
      port: 5000
      targetPort: web
  selector:
    name: $app_name
EOF
```

Here, we define a new service with `app_name`, that groups all Pods with name `$app_name` (this is defined by `selector:`). This Service on port `5000` allows balancing a traffic to all Pods `web` port (with round-rubin).

### 4.7. Ingress (add to `scripts/deploy`)

Ingress allows us to access the application from outside world.
This works like an Nginx's virtual host.

```bash
echo "Creating ingress to expose application service to outside world under $app_domain..."
cat <<EOF | kubectl apply --force -n $KUBE_NAMESPACE -f -
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: $app_name
  labels:
    app: $app_name
    job_id: "$CI_JOB_ID"
spec:
  rules:
  - host: $app_domain
    http:
      paths:
      - path: /
        backend:
          serviceName: $app_name
          servicePort: 5000
EOF
```

Here, we define a new Ingress, which on host `$app_domain` exposes a Service's port 5000.

### 4.8. Wait for deployment (add to `scripts/deploy`)

This is one of the most important parts of the script, as it allows us to wait for the deployment to finish and mark a CI job to be succeeded only when the deployment and its pods are actually running.

```bash
echo "Waiting for deployment..."
kubectl rollout status -n "$KUBE_NAMESPACE" -w "deployment/$app_name"
```

### 4.9. Configure GitLab

To actually make use of these scripts you have to [configure Kubernetes Integration](https://docs.gitlab.com/ce/user/project/integrations/kubernetes.html). 

You have to fill `namespace` to be your own. Set a namespace to be: `my-name-my-application`.
You can find credentials here: https://gitlab.com/clustereurope-gitlab/kubernetes-credentials/blob/master/README.md.

### 4.10. Commit changes and push to GitLab

Now, add a changes to `.gitlab-ci.yml` and to `scripts/deploy` and push to GitLab. If everything goes fine your Pipeline should create a `production` deployment on Kubernetes Cluster.

## 5. Next steps

Now, you have a working GitLab Pipeline with Docker Image building, with tests and Kubernetes deployment. You can go ahead and make it more robust. Try to:

- Make `app_image` to use Docker Layer Caching: `docker build --cache-from`,
- Define more stages: build, test, review, staging, production and:
- Make review job in review stage to:
  1. Have unique environment name and URL,
  2. Run only on [branches](https://docs.gitlab.com/ce/ci/yaml/README.html#only-and-except), but not [master](https://docs.gitlab.com/ce/ci/yaml/README.html#only-and-except),
- Make staging job in staging stage to:
  1. To use `staging` environment name and `http://staging-my-name.clustereurope-gitlab.xyz`,
  2. Run only on [master](https://docs.gitlab.com/ce/ci/yaml/README.html#only-and-except),
- Make production job in production stage to:
  1. Run only on [master](https://docs.gitlab.com/ce/ci/yaml/README.html#only-and-except),
  2. Make [a blocking manual action](https://docs.gitlab.com/ce/ci/yaml/README.html#manual-actions), so production deployment will not happen automatically when pushed to master, but when a button is pressed,
- Configure [GitLab Monitoring](https://docs.gitlab.com/ce/user/project/integrations/prometheus.html) using already provisioned [Prometheus instance](https://prometheus.clustereurope-gitlab.xyz/).

## 6. You did it all!

You got to this point and you made everything from 5., Great!

We do have yet another challenge for you:
1. Add [a special job](https://docs.gitlab.com/ce/ci/environments.html#stopping-an-environment) that will automatically remove review app once a branch is removed,
1. Add health checks to deployment to probe application for liveness and readiness and prevent broken deployments,
1. Create additional deployments to also deploy either PostgreSQL or MySQL and hook this database to your application,
1. Convert all handmade Kubernetes scripts to use [Helm, Kubernetes Package Manager](https://github.com/kubernetes/helm)

## 7. Voila!

Thanks for taking part in that journey and reaching that point. GitLab, 2017.

This is what is actually implemented currently in GitLab Auto-deploy:
https://gitlab.com/gitlab-examples/kubernetes-deploy.
